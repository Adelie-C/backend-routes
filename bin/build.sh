#!/bin/bash

# This script is used to set the API route in an env variable named VITE_API_URL,
# depending on the git branch we're on.
# As it is, it only works on NETLIFY.
# To use it on gitlab you'll need to change BRANCH variable name.

# RULES FOR NAMING BRANCHES :
#
# - Production branch should have a unique name, set in PREBUILD_PRODUCTION_BRANCH
#   => api route will be PREBUILD_PRODUCTION_API_URL
# - Staging branches should have a common prefix, set in PREBUILD_STAGING_BRANCH_PREFIX
#   => api route will be PREBUILD_STAGING_API_URL
# - Preprods branches should be built this way : 
#   - [prefix]/[whatever-you-want]
#     => api route will be https://[prefix].[preprods-domain]
#   or
#   - [prefix]-[special-preprod-subdomain]/[whatever-you-want]
#     => api route will be https://[special-preprod-subdomain].[preprods-domain]

# ENVIRONMENT VARIABLES :
#
# VITE_API_URL= (optional) if set, this script won't do anything.
# BRANCH= (required) SET BY NETLIFY ! should be the name of the branch you're building
#
# PREBUILD_PRODUCTION_BRANCH= (required) name of your production branch
# PREBUILD_PRODUCTION_API_URL= (required) api route for your production branch
# PREBUILD_STAGING_BRANCH_PREFIX= (required) prefix for your staging branches
# PREBUILD_STAGING_API_URL= (required) api route for your staging branches
# PREBUILD_PREPRODS_DOMAIN= (required) domain for your preprods branches

# ****************************************************
#                        CHECKS
# ****************************************************
# checking all environment variables are set (except for BRANCH, which should not exist in local environment)
if [[ -z "$PREBUILD_PRODUCTION_BRANCH" ]]; then
  echo "PREBUILD_PRODUCTION_BRANCH is not set. Please set it to your production branch name."
  exit 1
fi

if [[ -z "$PREBUILD_PRODUCTION_API_URL" ]]; then
  echo "PREBUILD_PRODUCTION_API_URL is not set. Please set it to your production API route."
  exit 1
fi

if [[ -z "$PREBUILD_STAGING_BRANCH_PREFIX" ]]; then
  echo "PREBUILD_STAGING_BRANCH_PREFIX is not set. Please set it to your staging branch name prefix (ex : 'goroco', 'staging' etc...)."
  exit 1
fi

if [[ -z "$PREBUILD_STAGING_API_URL" ]]; then
  echo "PREBUILD_STAGING_API_URL is not set. Please set it to your goroco API route."
  exit 1
fi

if [[ -z "$PREBUILD_PREPRODS_DOMAIN" ]]; then
  echo "PREBUILD_PREPRODS_DOMAIN is not set. Please set it to your preprods API domain."
  exit 1
fi

# ****************************************************
#            VITE_API_URL SET MANUALLY
# ****************************************************
# If VITE_API_URL is already set, we don't change it.
if [[ -n "$VITE_API_URL" ]]; then
  echo "Api route was set manually and takes priority over this script. It will stay $VITE_API_URL"
fi

# ****************************************************
#             Checking branch var is set
# ****************************************************
if [[ -z "$BRANCH" ]]; then
  echo "Variable for branch name is not set. Please make sure you run this script in the right environement..."
  exit 1
fi

# ****************************************************
#                        PROD
# ****************************************************
if [[ -z "$VITE_API_URL" && $BRANCH == "$PREBUILD_PRODUCTION_BRANCH" ]]; then
  # set PREBUILD_PRODUCTION_API_URL as API route
  echo "Branch is production branch. API route is set to : $PREBUILD_PRODUCTION_API_URL"
  export VITE_API_URL=$PREBUILD_PRODUCTION_API_URL
fi

# ****************************************************
#                       STAGING
# ****************************************************
if [[ -z "$VITE_API_URL" && $BRANCH == "$PREBUILD_STAGING_BRANCH_PREFIX"* ]]; then
  # set PREBUILD_STAGING_API_URL as API route
  echo "Branch is staging branch. API route is set to : $PREBUILD_STAGING_API_URL"
  export VITE_API_URL=$PREBUILD_STAGING_API_URL
fi

# ****************************************************
#                     PREPRODS 
# ****************************************************
if [[ -z "$VITE_API_URL" ]]; then
  # Extract branch prefix as subdomain. (in aca/666/my-branch, prefix is aca - in aca-1/545/feature-blah-blah, prefix is aca-1)
  SUBDOMAIN=$(echo "$BRANCH" | cut -d '/' -f 1)

  # if subdomain contains a -, and what is at the right of the - is not a digit, set subdomain to exactly what is at the right of the -
  # example : aca-master/111/super-feature => subdomain = master
  if [[ $SUBDOMAIN == *-* && ! ${SUBDOMAIN##*-} =~ ^[0-9]+$ ]]; then
    SUBDOMAIN=${SUBDOMAIN##*-}
  fi

  # Export the new API route as an environment variable
  export VITE_API_URL="https://$SUBDOMAIN.$PREBUILD_PREPRODS_DOMAIN"
fi

# ****************************************************
#                FINAL BUILD SCRIPT 
# ****************************************************
tsc
vite build